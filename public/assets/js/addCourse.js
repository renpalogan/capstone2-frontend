let addCourseForm = document.querySelector("#createCourse")


    addCourseForm.addEventListener("submit", (e) => {
        e.preventDefault()
    
        let courseName = document.querySelector("#courseName").value;
        let courseDescription = document.querySelector("#courseDescription").value;
        let coursePrice = document.querySelector("#coursePrice").value;
      
    
        //validation to enable create course button when all fields are populated 

      if((courseName !== '') && (courseDescription !== '') && (coursePrice !== '') ) {
          //check database
          //check for duplicate course in database first
          //url-where we can get the duplicate routes in our server
          //asynchronously load contents of the URL
          fetch(`https://shrouded-bayou-43101.herokuapp.com/api/courses/course-exists/`, {
              method: 'POST',
              headers: {
                  'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  'name': courseName
              })
          })
          //return a promise that resolves when res is loaded
          .then(res => res.json())//call this function when res is loaded
          .then(data =>{
            console.log(data)
              //if true(duplicate course)return alert
              //else fetching
              if(data === false){
              //if no duplicates found
              //get the routes for registration in our server
              const myToken = localStorage.getItem('token')
              fetch('https://shrouded-bayou-43101.herokuapp.com/api/courses/', {
                  method: 'POST',
                  headers: {
                    Authorization: `Bearer ${myToken}`,
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                      "name": courseName,
                      "description": courseDescription,
                      "price": coursePrice
                  })
              })
              .then(res => {
                  return res.json()
              })
              .then(data => {
                  console.log(data)
                  //if registration is successful
                  if(data === true){
                      alert("Course Added Successfully")
                      //redirect to login page
                      window.location.replace("./courses.html")
                   }
              })
              }else{
                  //error occured in registration
                alert("Course Already Exist")
              }
    
          })
      }else{
          alert("Something Went Wrong, Please try again")
          
      }
    
    
    });





