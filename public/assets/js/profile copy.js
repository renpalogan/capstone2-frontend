let adminUser = localStorage.getItem("token")
console.log(adminUser)
let params = new URLSearchParams(window.location.search);
console.log(window.location.search)

let userId = params.get('userId')
console.log(userId)

let userProfile = document.querySelector("#profileContainer") 

// get user details
    fetch(`http://localhost:3000/api/users/details/`, {
        headers: {
            Authorization: `Bearer ${adminUser}`,  
            
        }
    })
    .then(res => {
        return res.json()
    })
    .then(data => { console.log(data)
        let userDetails = data;
        if (data == 0) { return alert('No Data Found!')
        } else {
            // display user details
            userProfile.innerHTML = 
            `
            <div class="col-md-12">
                <div id="profileItems"></div> 
                <div class="bg-text">
                    <p class="text-center h5 font-weight-bold">
                        First Name: ${userDetails.firstName}
                    </p>
                    <p class="text-center h5 font-weight-bold">
                        Last Name: ${userDetails.lastName}
                    </p>
                    <p class="text-center h5 font-weight-bold">
                        Email: ${userDetails.email}
                    </p>
                    <p class="text-center h5 font-weight-bold">
                        Mobile Number: ${userDetails.mobileNo}
                    </p>

                    <p class="h3 font-weight-bold text-left">Class Enrolled</p>
                    <table class="w-100 my-5">
                        <thead>
                            <tr>
                                <th>Course ID</th>
                                <th class="text-center">Enrolled On</th>
                                <th class="text-right">Status</th>
                            </tr>
                            <tr>
                                <th>${userDetails.enrollments[0].courseId}</th>
                                <th class="text-center">${userDetails.enrollments[0].enrolledOn}</th>
                                <th class="text-right">${userDetails.enrollments[0].status}</th>
                            </tr>
                            <tr>
                        </thead>
                        <tbody id="tableBody">
                            
                        </tbody>
                    </table>                    

                </div>    
                
            </div>
            `
        }
    })

















    
/* erik code
<div class="column text-left" style="width: 40%">
<h6>Course ID</h6>
</div>
<div class="column" style="width: 15%">
    <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enrolled On</h6>
</div>
<div class="column" style="width 45%">
    <h6 class="text-left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status</h6>
</div>
</div>
<table class="w-100 my-1">
<thead class="d-inline-block">
    <tr class="tableHeader">

    </tr>
</thead>
<tbody id="tBody" class="d-inline-block" w-100>

</tbody>
</table> */

// let adminUser = localStorage.getItem("token")

// let params = new URLSearchParams(window.location.search);

// let userId = params.get('userId')
// console.log(userId)

// let userProfile = document.querySelector("#profileContainer") 

// let userEnrollments;

// // get user details
// fetch(`http://localhost:3000/api/users/details/`, {
//     headers: {
//         Authorization: `Bearer ${adminUser}`,  
        
//     }
// })
// .then(res => res.json())
// .then(data => {  //console.log(data)

// 	//log the data to check if you were able to fetch the data from our server
//     let userDetails;
// 	//if the number of courses fetched is less than 1, display no courses available
// 	if(data == 0){ 
// 		return alert('No Data Found!')
// 	}else{
// 		//else iterate the courses collection and display each course
//         userDetails = data.map( enrollments => { 
//             console.log(userDetails)
// 		//if the user is a regular user, display when the course was created and display the button, select course
// 		if(adminUser == "false" || !adminUser){
//             userEnrollments = 
//             `
//             <table class="w-100 my-5">
//             <thead>
//                 <tr>
//                     <th>Course ID</th>
//                     <th class="text-center">Enrolled On</th>
//                     <th class="text-right">Status</th>
//                 </tr>
//                 <tr>
//                     <th>${enrollments[0].courseId}</th>
//                     <th class="text-center">${enrollments[0].enrolledOn}</th>
//                     <th class="text-right">${enrollments[0].status}</th>
//                 </tr>
//                 <tr>
//             </thead>
//             <tbody id="tableBody">
                
//             </tbody>
//         </table>  

//             `
// 		}else{fetch(`http://localhost:3000/api/courses/`, {
//             headers: {
//                 Authorization: `Bearer ${adminUser}`,     
//             }
//         })
//         .then(res => {
//             return res.json()
//         })
//         .then(data => { console.log(data) 
        
//         })

		

//         }
		
// 			return(
// 				`
//             <div class="col-md-12">
//                 <div id="profileItems"></div> 
//                 <div class="bg-text">
//                     <p class="text-center h5 font-weight-bold">
//                         First Name: ${userDetails.firstName}
//                     </p>
//                     <p class="text-center h5 font-weight-bold">
//                         Last Name: ${userDetails.lastName}
//                     </p>
//                     <p class="text-center h5 font-weight-bold">
//                         Email: ${userDetails.email}
//                     </p>
//                     <p class="text-center h5 font-weight-bold">
//                         Mobile Number: ${userDetails.mobileNo}
//                     </p>

//                     <p class="h3 font-weight-bold text-left">Class Enrolled</p>
//                     <table class="w-100 my-5">
//                         <thead>
//                             <tr>
//                                 <th>Course ID</th>
//                                 <th class="text-center">Enrolled On</th>
//                                 <th class="text-right">Status</th>
//                             </tr>
//                             <tr>
//                                 <th>${userDetails.enrollments.courseId}</th>
//                                 <th class="text-center">${userDetails.enrollments.enrolledOn}</th>
//                                 <th class="text-right">${userDetails.enrollments.status}</th>
//                             </tr>
//                             <tr>
//                         </thead>
//                         <tbody id="tableBody">
                            
//                         </tbody>
//                     </table>                    

//                 </div>    
                
//             </div>
//             `
// 			)
// 		}).join("")
// 		//since the collection is an array, we can use join to indicate the separator of each element to replace the comma

// 		let container = document.querySelector("#profileContainer")

// 		container.innerHTML = userDetails
// 	}
// })



// let adminUser = localStorage.getItem("token")
// let userProfile = document.querySelector("#profileContainer") 
// let params = new URLSearchParams(window.location.search);
// console.log(window.location.search)



// if(!adminUser) {alert ('Something went wrong')
// }else {
//     fetch(`http://localhost:3000/api/users/details/`, {
//     headers: {
//         Authorization: `Bearer ${adminUser}`,  
//     }
//     }).then(res => res.json())
//     .then( userData => {  console.log(userData)
//         if(!userData) return false
//         else {
//             userProfile.innerHTML = 
//             `
//             <div class="col-md-12">
//             <main class="container my-5">
//                 <div id="profileContainer" class="row">
//                     <div class="col-md-12">
                        
//                         <div id="profileItems"></div>
//                         <div class="bg-text jumbotron text-center my-5">
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             First Name: ${userData.firstName}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Last Name: ${userData.lastName}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Email: ${userData.email}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Mobile Number: 0${userData.mobileNo}
//                         </p>
//                         <h3 class="my-2 font-weight-bold text-left">Class Enrolled</h3>
//                         <div class="row" style="margin-left: 0%" style="margin-bottom: 0%">
//                         <table class="w-100 my-5">
//                         <thead>
//                             <tr>
//                                 <th>Course ID</th>
//                                 <th class="text-center">Enrolled On</th>
//                                 <th class="text-right">Status</th>
//                             </tr>
//                         </thead>
//                         <thead class="d-inline-block">
//                         <tr class="tableHeader">
                    
//                         </tr>
//                         </thead>
//                         <tbody id="tBody" class="d-inline-block" w-100>
                    
//                         </tbody>
//                         </table> 
//                         </div>  
                        
//                     </div>
//                 </div>
//             </main>
//         </div>

//        `

//     let courseDetails = document.querySelector('#tBody');
//     userData.enrollments.map(userInfo => {
//         if (!userInfo) {alert ('No data found!')
//     } else { fetch(`http://localhost:3000/api/courses/`)
//         .then(res => res.json())
//         .then( courseData => {  console.log(courseData)
//             if (courseData.length < 1) alert ('There is no such course')
//             courseData.map(course => { //console.log(courseData)
//                 if(course.id === userInfo.courseId) {
//                     let dc = userInfo.enrolledOn
//                     id = dc.substr(0, 10)
//                     return courseDetails.innerHTML += (
//                         `
//                         <tr class="" style="width: 100%">
//                             <td class="py-lg-2 border-top border-bottom text-left" 
//                             style="width: 40%"> ${course.name}</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom"
//                             style="color: rgb(51, 51, 51);">abcde</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom"
//                             style="width: 15% color: rgb(51, 51, 51);">${id}</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom text-rights"
//                             style="width: 45% color: rgb(51, 51, 51);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
//                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${userInfo.status}</td>
//                         </tr>
//                         `
                        
//                     )
//                 }
//             }).join("")
//         })
//             }
//     })
//             }
// })
// }





// let token = localStorage.getItem('token');
// let userProfile = document.querySelector('#profileContainer');

// if (!token) alert('Something went wrong')
// else {
//     fetch('http://localhost:3000/api/users/details', {
//         headers: {
//             Authorization: `Bearer ${token}`
//         }
//     })
//     .then(res => res.json())
//     .then(userData => {
//         if (!userData) return false;
//         else {
//             userProfile.innerHTML = 
//                 `
//                 <div class="col-md-12">
//                     <div class=" userInfo">
//                         <p class="text-center font-weight-bold">
//                             First Name: ${userData.firstName}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Last Name: ${userData.lastName}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Email: ${userData.email}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Mobile Number: ${userData.mobileNo}
//                         </p>
//                         <p class="h3 font-weight-bold">
//                             Class History
//                         </p>
//                         <table class="w-100 my-5">
//                             <thead class="d-inline-block thead__content w-100">
//                                 <tr class="tableHeader">
//                                     <th class="mb-lg-5 courseId text-left">Course ID</th>
//                                     <th class="mb-lg-5 header__enrolled">Enrolled On</th>
//                                     <th class="mb-lg-5 header__status">Status</th>
//                                 </tr>
//                             </thead>
//                             <tbody id="tBody" class="d-inline-block w-100">
                                
//                             </tbody>
//                         </table>
//                     </div>
//                 </div>
//                 `
//             let table = document.querySelector('#tBody');
//             userData.enrollments.map(userInfo => {
//                 if (!userInfo) alert('No such data found!')
//                 fetch('http://localhost:3000/api/courses')
//                 .then(result => result.json())
//                 .then(courseData => {
//                     if (courseData.length < 1) alert('There is no such course')
//                     courseData.map(course => {
//                         if(course._id === userInfo.courseId) {
//                             return table.innerHTML += (
//                                 `
//                                 <tr class="tableRow">
//                                     <td class="py-lg-3 border-top border-bottom w-100 text-left">${course.name}</td>
//                                     <td class="py-lg-3 border-top text-centers border-bottom">${userInfo.enrolledOn}</td>
//                                     <td class="py-lg-3 border-top border-bottom text-rights">${userInfo.status}</td>
//                                 </tr>
//                                 `
//                             ) + '<br/>'
//                         }
//                     }).join('')
//                 })
//             })
            
//         }
//     })
// }

// let adminUser = localStorage.getItem("token")
// let userProfile = document.querySelector("#profileContainer") 

// if(!adminUser) alert ('Something went wrong')
// else {
//     fetch(`http://localhost:3000/api/users/details/`, {
//     headers: {
//         Authorization: `Bearer ${adminUser}`,  
//     }
//     }).then(res => res.json())
//     .then( userData => {  console.log(userData)
//         if(!userData) return false
//         else {
//             userProfile.innerHTML =
//             `
//             <div class="col-md-12">
//             <main class="container my-5">
//                 <div id="profileContainer" class="row">
//                     <div class="col-md-12">
//                         <section class="jumbotron text-center my-5">
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             First Name: ${userData.firstName}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Last Name: ${userData.lastName}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Email: ${userData.email}
//                         </p>
//                         <p class="my-2 text-center h5 font-weight-bold">
//                             Mobile Number: 0${userData.mobileNo}
//                         </p>
//                         <h3 class="my-2 font-weight-bold text-left">Class Enrolled</h3>
//                         <div class="row" style="margin-left: 0%" style="margin-bottom: 0%">
//                                 <div class="column text-left" style="width: 40%">
//                                     <h6>Course ID</h6>
//                                 </div>
//                                 <div class="column" style="width: 15%">
//                                     <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enrolled On</h6>
//                                 </div>
//                                 <div class="column" style="width 45%">
//                                     <h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status</h6>
//                                 </div>
//                             </div>
//                             <table class="w-100 my-1">
//                                 <thead class="d-inline-block">
//                                     <tr class="tableHeader">
    
//                                     </tr>
//                                 </thead>
//                                 <tbody id="tBody" class="d-inline-block" w-100>
    
//                                 </tbody>
//                             </table>
//                         </section>
//                     </div>
//                 </div>
//             </main>
//         </div>

//        `

//     let table = document.querySelector('#tBody');
//     userData.enrollments.map(userInfo => {
//         if (!userInfo) alert ('No data found!')
//         fetch(`http://localhost:3000/api/courses/`)
//         .then(res => res.json())
//         .then( courseData => {  console.log(courseData)
//             if (courseData.length < 1) alert ('There is no such course')
//             courseData.map(course => {
//                 if(course.id === userInfo.courseId) {
//                     let dc = userInfo.enrolledOn
//                     id = dc.substr(0, 10)
//                     return table.innerHTML += (
//                         `
//                         <tr class="" style="width: 100%">
//                             <td class="py-lg-2 border-top border-bottom text-left" 
//                             style="width: 40%"> ${course.name}</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom"
//                             style="color: rgb(51, 51, 51);">abcde</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom"
//                             style="width: 15% color: rgb(51, 51, 51);">${id}</td>
//                             <td class="py-lg-2 border-top text-centers border-bottom text-rights"
//                             style="width: 45% color: rgb(51, 51, 51);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
//                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${userInfo.status}</td>
//                         </tr>
//                         `
                        
//                     )
//                 }
//             }).join("")
//         })

//     })
//         }
// })
// }