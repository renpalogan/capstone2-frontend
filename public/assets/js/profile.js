let adminUser = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")
let userProfile = document.querySelector("#profileContainer") 
let params = new URLSearchParams(window.location.search);
console.log(window.location.search)



if(adminUser == 'true') {

    fetch(`https://shrouded-bayou-43101.herokuapp.com/api/users/details/`, {
    headers: {
        Authorization: `Bearer ${token}`,  
    }
    }).then(res => res.json())
    .then( userData => {  console.log(userData)
        if(!userData) alert ('no data')
        else {
     userProfile.innerHTML = 
            `
                        <div class="col-md-12">
                        <div id="profileContainer" class="row">
                        <div class="col-md-12">
                            
                            <div id="profileItems"></div>
                            <div class="bg-text jumbotron my-5">
                            <h2 class="text-left">Admin</h2>
                            <p class="my-2 text-center h5 font-weight-bold">
                                First Name:   ${userData.firstName}
                            </p>
                            <p class="my-2 text-center h5 font-weight-bold">
                                Last Name:   ${userData.lastName}
                            </p>
                            <p class="my-2 text-center h5 font-weight-bold">
                                Email:   ${userData.email}
                            </p>
                            <p class="my-2 text-center h5 font-weight-bold">
                                Mobile Number: 0${userData.mobileNo}
                            </p>

  
                                </div>
                            </div>
                        </div>

       `
        }
    })

}else {
    fetch(`https://shrouded-bayou-43101.herokuapp.com/api/users/details/`, {
    headers: {
        Authorization: `Bearer ${token}`,  
    }
    }).then(res => res.json())
    .then( userData => {  console.log(userData)
        if(!userData) return false
        else {
            userProfile.innerHTML = 
            `
                        <div class="col-md-12">
                            <div id="profileContainer" class="row">
                                <div class="col-md-12">
                                    
                                    <div id="profileItems"></div>
                                    <div class="bg-text jumbotron text-center my-5">
                                    <p class="my-2 text-center h5 font-weight-bold">
                                        First Name: ${userData.firstName}
                                    </p>
                                    <p class="my-2 text-center h5 font-weight-bold">
                                        Last Name: ${userData.lastName}
                                    </p>
                                    <p class="my-2 text-center h5 font-weight-bold">
                                        Email: ${userData.email}
                                    </p>
                                    <p class="my-2 text-center h5 font-weight-bold">
                                        Mobile Number: 0${userData.mobileNo}
                                    </p>
                                    <h3 class="my-2 font-weight-bold text-left">Class Enrolled</h3>
                                    <div class="row" style="margin-left: 0%" style="margin-bottom: 0%">
                                    <table class="w-100 my-5">
                                    <thead>
                                        <tr>
                                            <th class="text-left">Course ID</th>
                                            <th class="text-center">Enrolled On</th>
                                            <th class="">Status</th>
                                        </tr>
                                    </thead>
                                    <thead class="d-inline-block">
                                    <tr class="tableHeader">
                                
                                    </tr>
                                    </thead>
                                    <br>
                                    <tbody id="tBody">
                                
                                    </tbody>
                                    </table> 
                                    </div>  
                                    
                                </div>
                            </div>
                        </div>

       `

let table = document.querySelector('#tBody');
userData.enrollments.map(userInfo => {
    if (!userInfo) alert('No such data found!')
    fetch('https://shrouded-bayou-43101.herokuapp.com/api/courses')
    .then(result => result.json())
    .then(courseData => { console.log(courseData)
        if (courseData.length < 1) alert('There is no such course')
        courseData.map(course => { console.log(userInfo)
            if(course._id === userInfo.courseId) {

                let x = userInfo.enrolledOn
                userInfoSubstr = x.substr(0, 10)
                return table.innerHTML += (
                    `
                    <tr class="">
                        <td class=" text-left">${course.name}</td>
                        <td class=" text-centers ">${userInfoSubstr}</td>
                        <td class="text-rights">${userInfo.status}</td>
                    </tr>
                    `
                ) 
            }
        }).join('')
    })
})

}
})
}


// + '<br/>'

// let token = localStorage.getItem('token');
// let userProfile = document.querySelector('#profileContainer');

// if (!token) alert('Something went wrong')
// else {
//     fetch('http://localhost:3000/api/users/details', {
//         headers: {
//             Authorization: `Bearer ${token}`
//         }
//     })
//     .then(res => res.json())
//     .then(userData => {
//         if (!userData) return false;
//         else {
//             userProfile.innerHTML = 
//                 `
//                 <div class="col-md-12">
//                     <div class=" userInfo">
//                         <p class="text-center font-weight-bold">
//                             First Name: ${userData.firstName}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Last Name: ${userData.lastName}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Email: ${userData.email}
//                         </p>
//                         <p class="text-center font-weight-bold">
//                             Mobile Number: ${userData.mobileNo}
//                         </p>
//                         <p class="h3 font-weight-bold">
//                             Class History
//                         </p>
//                         <table class="w-100 my-5">
//                             <thead class="d-inline-block thead__content w-100">
//                                 <tr class="tableHeader">
//                                     <th class="mb-lg-5 courseId text-left">Course ID</th>
//                                     <th class="mb-lg-5 header__enrolled">Enrolled On</th>
//                                     <th class="mb-lg-5 header__status">Status</th>
//                                 </tr>
//                             </thead>
//                             <tbody id="tBody" class="d-inline-block w-100">
                                
//                             </tbody>
//                         </table>
//                     </div>
//                 </div>
//                 `
//             let table = document.querySelector('#tBody');
//             userData.enrollments.map(userInfo => {
//                 if (!userInfo) alert('No such data found!')
//                 fetch('http://localhost:3000/api/courses')
//                 .then(result => result.json())
//                 .then(courseData => {
//                     if (courseData.length < 1) alert('There is no such course')
//                     courseData.map(course => {
//                         if(course._id === userInfo.courseId) {
//                             return table.innerHTML += (
//                                 `
//                                 <tr class="tableRow">
//                                     <td class="py-lg-3 border-top border-bottom w-100 text-left">${course.name}</td>
//                                     <td class="py-lg-3 border-top text-centers border-bottom">${userInfo.enrolledOn}</td>
//                                     <td class="py-lg-3 border-top border-bottom text-rights">${userInfo.status}</td>
//                                 </tr>
//                                 `
//                             ) + '<br/>'
//                         }
//                     }).join('')
//                 })
//             })
            
//         }
//     })
// }



