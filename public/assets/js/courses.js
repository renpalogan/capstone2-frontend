let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter;

if(adminUser == "false" || !adminUser){
	//if the user is a regular user, do not show the add course button
	modalButton.innerHTML = null
}else{
	//display add course button if user is an admin
	modalButton.innerHTML =
	`
	<div class="col-md-2 offset-md-10">
		<a href="./addCourse.html" class="btn btn-block btn-success">Add Course</a>
	</div>
	`
}

//fetch courses from API

fetch('https://shrouded-bayou-43101.herokuapp.com/api/courses/')
.then(res => res.json())
.then(data => {
	// console.log(data)
	//log the data to check if you were able to fetch the data from our server
	let courseData;
	//if the number of courses fetched is less than 1, display no courses available
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
        courseData = data.map(course => {
		//if the user is a regular user, display when the course was created and display the button, select course
		if(adminUser == "false" || !adminUser){
			cardFooter = 
			`
				<a href="./course.html?courseId=${course._id}" value={course._id} class="btn btn-warning text-white btn-block viewButton">
				Select Course
				</a>
			`
		}else{
		//else, if the user is admin, display the edit and delete button
		//when the edit button is clicked, it will redirect the user to editCourse.html(name of button=Edit/Update)
		//when the delete button is clicked, the course will be disabled(name of button=Disable Course)
		//key = courseId
        //value = the element's courseId
        // ?courseId=${course._id} = to redirect to specific course id
            cardFooter = 
            `
            
				<a href="./editCourse.html?courseId=${course._id}" value={course._id} class="btn btn-primary text-white btn-block editButton"> Edit
				</a>
			
				<a href="./deleteCourse.html?courseId=${course._id}" value={course._id} class="btn btn-danger text-white btn-block deleteButton"> Disable
                </a>
            
			`
		}


		
			return(
				`
				
				<div class="col-md-6 my-3">
				
					<div class="card" id="courses-card">
						<div class="card-body-courses">
							<h5 class="card-titles">${course.name}</h5>
							<p class="card-texts text-left">
								${course.description}
							</p>
							<p class="card-texts text-right">
								Price: ₱ <span id="coursePrice">${course.price}</span> 	
							</p>
						</div>
						<div class="card-footer">
						${cardFooter}
						</div>
					</div>
				
				</div>
				`
			)
		}).join("")
		//since the collection is an array, we can use join to indicate the separator of each element to replace the comma

		let container = document.querySelector("#coursesContainer")

		container.innerHTML = courseData
	}
})