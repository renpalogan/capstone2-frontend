let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let deleteUserToken = localStorage.getItem("token")

fetch(`https://shrouded-bayou-43101.herokuapp.com/api/courses/${courseId}`, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${deleteUserToken}`
                }
            }).then(res => {
                return res.json()
            }).then(result => {
                alert("Course disabled")
                console.log(result)
                window.location.replace("./courses.html")
            })